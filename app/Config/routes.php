<?php

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'users', 'action' => 'home'));
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/new_event', array('controller' => 'events', 'action' => 'create'));
	Router::connect('/my_events', array('controller' => 'users', 'action' => 'my_events'));
	Router::connect('/browse', array('controller' => 'categories', 'action' => 'browse_all'));
	Router::connect('/event/:id', array('controller' => 'events', 'action' => 'view'), array('id'=> '[0-9]+', 'pass' => array('id')));
	Router::connect('/category/:id', array('controller' => 'categories', 'action' => 'browse'), array('id'=> '[0-9]+', 'pass' => array('id')));
	Router::connect('/explore', array('controller' => 'events', 'action' => 'explore'));
	Router::connect('/explore/:id', array('controller' => 'events', 'action' => 'explore'), array('id' => '[0-9]+', 'pass' => array('id')));
	Router::connect('/explore_list/:lat/:long', array('controller' => 'events', 'action' => 'explore_list'), array('pass' => array('lat', 'long')));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
