<?php

class AppController extends Controller {
	public $components = array(
		'Session',
		'Auth' => array(
			'loginRedirect' => array('controller'=>'users', 'action'=>'home'),
			'logoutRedirect' => array('controller'=>'users', 'action'=>'home'),
			'authError' => 'You cannot access this page.',
			'authorize' => array('Controller')
		)
	);
	
	public function isAuthorized($user) {
		return true;
	}
	
	public function beforeFilter() {
		$this->Auth->allow('index', 'view');
	}
	
	public function beforeRender() {
		
		$this->set('logged_in', $this->Auth->loggedIn());
		$this->set('current_user', $this->Auth->user());
	}
	
	
}

?>