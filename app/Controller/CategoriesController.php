<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController {

	public function browse($id = null) {
			$this->set('category_list', $this->Category->find('all'));
	}

	public function browse_nested($id = null) {
		if ($id == null || $id == 0) {
			$this->set('child_category_list', $this->Category->findAllByParentId('0'));
		} else {
			$this->Category->id = $id;
			if (!$this->Category->exists()) {
				throw new NotFoundException(__('Invalid category'));
			}
			$this->set('current_category', $this->Category->read(null, $id));
			$this->set('child_category_list', $this->Category->findAllByParentId($id));
		}
	}
	
}

