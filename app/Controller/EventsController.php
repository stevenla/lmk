<?php
App::uses('AppController', 'Controller');

class EventsController extends AppController {

	public $components = array('RequestHandler');      

	private function send_texts($cat)
	{
	}

	public function view($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		$this->set('event', $this->Event->read(null, $id));
	}

	public function create() {
		$categories = $this->Event->Category->find('list');
		$this->set(compact('categories'));
	}

/*
	public function add() {
		if ($this->request->is('post')) {
			$this->Event->create();
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Event->Category->find('list');
		$this->set(compact('categories'));
	}
*/

	public function edit($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('/my_events'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Event->read(null, $id);
		}
		$categories = $this->Event->Category->find('list');
		$this->set(compact('categories'));
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->Event->delete()) {
			$this->Session->setFlash(__('Event deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Event was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function ajax_add() {
		$this->autoRender = false; 
		
		if($this->RequestHandler->isAjax()){ 
			Configure::write('debug', 0); 
		} 
		
		if(!empty($this->data)) {
     		$this->request->data['Event']['start_time'] = $this->request->data['Event']['start_time_a'];
			$this->Event->create($this->request->data);
     		$this->Event->start_time = $this->request->data['Event']['start_time_a'];
     		$this->request->data['Event']['user_id'] = $this->Auth->user('id');
			if ($this->Event->save($this->request->data)) {
				$data['MSG']='OK';
					
/*						
				require "Services/Twilio.php";
				$AccountSid = "AC14d2aada9651401abc2294101ab540e1";
				$AuthToken = "009fb551f1c1726d27ac19b4556226c3";
				$client = new Services_Twilio($AccountSid, $AuthToken);
				$this->LoadModel('Subscription');
				$this->LoadModel('User');
				$cat = $this->request->data['Event']['Category.id'];
				$this->Subscription->recursive = 1;
				$subs_full_arr = $this->Subscription->findAllByCategoryId($cat);
				$subs_arr = array();
				
				foreach($subs_arr as $sub)
				{
					if($this->calculateDistance($this->request->data['Event']['latitude'], $this->request->data['Event']['longitude'],
						$sub['Subscription']['latitude'], $sub['Subscription']['longitude']) <= 2)		
					{
						$subs_arr[] = $sub;
					} 
				}
				
				$params = array(
					'conditions' => array(
						$sub['Subscription']['user_id'] => $user['User']['id']
						),
					'fields' => array('User.id', 'User.phone'),
				);
				$subscribed_users = $this->User->find('all', $params);
				foreach($subs_arr as $sub)
				{
					$msg = "There's a new".$sub['Subscription']['Category']['name']." at ";
					$sms = $client->account->sms_messages->create("415-599-2671", $subscribed_users['User']['id'], $msg);
				}*/
				
			} else {
				$data['MSG']='NO';
			}
			echo json_encode($data);
		}
	}
	
	public function explore($sid= null) {
		$this->LoadModel('Categories');
		if ($sid != null) {
			$this->LoadModel('Subscription');
			$sub = $this->Subscription->findById($sid);
			if ($sub['Subscription']['user_id'] == $this->Auth->user('id')) {
				$this->set('subscription', $sub);
			}
		}
		$this->set('category_list', $this->Categories->find('all'));
	}
	
	public function explore_list($lat, $long) {
		$this->LoadModel('Categories');
		$this->set('category_list', $this->Categories->find('all'));
		$this->LoadModel('Events');
		$event_list = $this->Event->find('all');
		foreach ($event_list as &$event)
		{
			$event['Event']['miles_from_me'] = $this->calculateDistance($lat, $long, $event['Event']['latitude'], $event['Event']['longitude']);
		}
		$this->set('event_list', $event_list);
	}
	
	public function ajax_around_me() {
		$this->autoRender = false; 
		
		if($this->RequestHandler->isAjax()){ 
			Configure::write('debug', 0); 
		} 
		if(!empty($this->data)) {
			$cat = $this->data['filter_cat_id'];
			$long1 = $this->data['cur_long'];
			$lat1 = $this->data['cur_lat'];
			//$cat = $this->params->pass[0];
			//$long1 = $this->params->pass[1];
			//$lat1 = $this->params->pass[2];
			//$lat1 = 34.022609;
			//$long1 =  -118.286700;
			$this->Event->recursive = -1;
			$event_arr_full = $this->Event->findAllByCategoryId($cat);
			$event_arr = array();
			foreach ($event_arr_full as $event) {
				if ($this->calculateDistance($lat1, $long1, $event['Event']['latitude'], $event['Event']['longitude']) <= 2)
				{
					$event_arr[] = $event;
				}
			}
			
			echo json_encode($event_arr);
		}
	}
	
	private function calculateDistance($lat1, $long1, $lat2, $long2) //in miles
	{
    	return (6378.1 * acos(sin($lat1/57.2958)* sin($lat2/57.2958) + cos($lat1/57.2958) * cos($lat2/57.2958) * cos($long2/57.2958 - $long1/57.2958))) * 0.621371192;
	}
}
