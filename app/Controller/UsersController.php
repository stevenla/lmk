<?php

class UsersController extends AppController {

	public $components = array('RequestHandler');   
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('home', 'add', 'logout');
	}

	public function index() {
		if ($this->Auth->loggedIn()) {
			$this->redirect('/');
		}
	}

	public function home() {
		$this->LoadModel('Subscription');
		$this->Subscription->recursive = 1;
		$this->set('subscription_arr', $this->Subscription->findAllByUserId($this->Auth->user('id')));
	}

	public function login() {
		if ($this->Auth->loggedIn()) {
			$this->redirect('/');
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash('Your username and password combination was incorrect.');
			}
		}
	}
	
	public function logout() {
		$this->redirect($this->Auth->logout());
	}

	public function view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	public function my_events() {
		$this->LoadModel('Event');
		$this->set('event_arr', $this->Event->findAllByUserId($this->Auth->user('id')));
	}
	
	public function ajax_subscribe() {
		$this->autoRender = false; 
		
		if($this->RequestHandler->isAjax()){ 
			Configure::write('debug', 0); 
		} 
		if(!empty($this->data)){
			$this->request->data['Subscription']['user_id'] = $this->Auth->user('id');
			if ($this->User->Subscription->save($this->request->data)) {
				$data['MSG']='OK';
			} else {
				$data['MSG']='NO';
			}
			echo json_encode($data);
		}
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('Your account has been created.');
				if ($this->Auth->login()) {
					$this->redirect($this->Auth->redirect());
				}
			} else {
				$this->Session->setFlash('Please check the errors and try again.');
			}
		}
	}
/*
	public function edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
	}
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
*/
}
