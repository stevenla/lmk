<?php 
/**
 * Twilio DataSource
 *
 * Used for reading and writing to Twilio, through models.
 *
 * PHP Version 5.x
 *
 *
 * @filesource
 * @copyright     Copyright 2010, Life Is Content
 * @link          http://lifeiscontent.net Life Is Content
 */
App::import('Core', 'HttpSocket');
class TwilioSource extends DataSource {
    private $ApiVersion = "2008-08-01";
    protected $_schema = array(
        'texts' => array(
            'From' => array(
                'type' => 'integer',
                'null' => true,
                'key' => 'primary',
                'length' => 10,
            ),
            'To' => array(
                'type' => 'integer',
                'null' => true,
                'key' => 'primary',
                'length' => 10
            ),
            'Body' => array(
                'type' => 'string',
                'null' => true,
                'key' => 'primary',
                'length' => 160
            ),
        )
    );
    public function __construct($config) {
        $auth = "{$config['AccountSid']}:{$config['AuthToken']}";
        $this->connection = new HttpSocket(
            "https://{$auth}@api.twilio.com/"
        );
        parent::__construct($config);
    }
    public function listSources() {
        return array('texts');
    }
    public function read($model, $queryData = array()) {
        $url = "/".$this->ApiVersion."/Accounts/";
        $url .= "{$this->config['AccountSid']}/SMS/Messages.json";
        $response = json_decode($this->connection->get($url), true);
        $results = array();
        foreach ($response['TwilioResponse']['SMSMessages'] as $record) {
            $record = array('Text' => $record['SMSMessage']);
            $results[] = $record;
        }
        return $results;
    }
    public function create($model, $fields = array(), $values = array()) {
        $data = array_combine($fields, $values);
        $result = $this->connection->post("/".$this->ApiVersion."/Accounts/{$this->config['AccountSid']}/SMS/Messages.json", $data);
        $result = json_decode($result, true);
        debug($result);
        if (isset($result['Sid'])) {
            $model->setInsertId($result['Sid']);
            return true;
        }
        return false;
    }
    public function describe($model) {
        return $this->_schema['texts'];
    }
}
?> 