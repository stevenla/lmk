<label for="category_search">Search</label><input id="category_search" type="text" style="line-height: 20px; width: 100%;" />
<ul class="list">
	<lh>What are you interested in?</lh>
<?php foreach( $child_category_list as $category): ?>
	<li><a href="/category/<?php echo $category['Category']['id'];?>"><strong><?php echo $category['Category']['name']; ?></a></strong></li>
<?php endforeach; ?>
</ul>
<?php if ($this->params['id'] != 0): ?>
	<a href="<?php echo $this->Html->url('/category/'.$current_category['Category']['parent_id']);?>">Back</a>
<?php endif; ?>