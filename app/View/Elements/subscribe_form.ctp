<?php echo $this->Form->create('Subscription', array('class' => 'eventForm'));?>
<p>Subscribe to a category and an area to recieve text message notifications of people hosting events in that category and area</p>
<br>
<fieldset>
<?php
	echo $this->Form->input('name', array('label' => 'Area Label'));
	echo $this->Form->input('longitude', array('type' => 'hidden'));
	echo $this->Form->input('latitude', array('type' => 'hidden'));
	echo $this->Form->input('radius', array('type' => 'hidden'));
	echo $this->Form->input('category_id', array('type' => 'hidden'));
?>
</fieldset>
<input type="button" value="Cancel" class="left" id="cancel"/>
<?php echo $this->Form->submit('Subscribe'); ?>
<div class="clear"></div>
<?php echo $this->Form->end();?>
