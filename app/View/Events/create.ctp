<?php $this->set('tab_bar', '<span id="navright"><a href="#" id="new_event">Create</a></span>'); ?>
 
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1MJxRNYKyb_-ONBOJt1wUHe03niXTxPw&sensor=true"> 
    </script>

    <script type="text/javascript">

		$(document).ready(function() {
			$('#search_bar').live('submit', function(e){
				codeAddress();
				$('#address').blur();
				e.preventDefault();
			});

		    $('#cancel').click(function(){
				$("#eventForm").animate({"top": "-400px"}, "slow");
		    	cleanForm();
		    	return false;
		    });

			var alerted = false;
			$('#new_event').click(function() {
				createEvent = true;
				$('#navright').addClass('pressed');
				if (!alerted)
				{
					alert("Tap a location on the map to set");
					alerted = true;
				}
			});

		    $('#EventCreateForm').submit(function(e){ 
				var formData = $(this).serialize(); 
				var formUrl = "/events/ajax_add"; 
				$.ajax({ 
					type: 'POST', 
					url: formUrl, 
					data: formData, 
					success: function(data)
					{ 
						data = JSON.parse(data);
						if(data.MSG == "OK"){
							$("#eventForm").animate({"top": "-400px"}, "slow");
							var pos = new google.maps.LatLng($('#EventLatitude').val(),
                                            $('#EventLongitude').val());
							addMarker(pos,"Event");
							cleanForm();
						}
						else
							alert("Can't create event. Try again!");
						
					}
				});
				e.preventDefault()
			});
			
			$('#currentLocation').click(function(){
				 map.panTo(homeView);
			});	
		  });

		function cleanForm(){
			$(':input','#EventCreateForm')
			 .not(':button, :submit, :reset, :hidden, :select')
			 .val('');
			alert($('#EventName').val());
		}
		
		var map;
		var markersArray = [];
		var createEvent = false;
		var geocoder;
		
		/*Initialize*/
		
		function initialize() {
			//var defaultView = new google.maps.LatLng(37.7699298, -122.4469157);
		    geocoder = new google.maps.Geocoder();
			var mapOptions = {
		    	zoom: 14,
		    	//center: defaultView,
		    	mapTypeControl: false,
		    	streetViewControl: false,
				scaleControl: false,
				zoomControl: false,
				panControl: false,
		    	mapTypeId: google.maps.MapTypeId.ROADMAP
		  	};
		  	map =  new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
			
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(success, error);
			} else {
				alert('geolocation not supported');
			}
		  
		    //listener functions for creating events
			google.maps.event.addListener(map, 'click', function(e) {
				if(createEvent == true){
			    	$("#eventForm").animate({"top": "0"}, "slow");
			    	//presave the lat/lng value
			    	
			    	$('#EventLongitude').val(e.latLng.lng());
			    	$('#EventLatitude').val(e.latLng.lat());
			    	createEvent = false;
					$('#navright').removeClass('pressed');
			    }
			});
		}
		var searchMarker;
		//search location
		function codeAddress() {
			 var address = $("#address").val();
			 //window.alert(address);
			geocoder.geocode( { 'address': address}, function(results, status) {
				
				if (status == google.maps.GeocoderStatus.OK) {

					if(!searchMarker){
						searchMarker=addMarker(results[0].geometry.location)
					}else
					{
						
						searchMarker.setPosition(results[0].geometry.location);
					}			  	} else {
			    	window.alert("Geocode was not successful for the following reason: " + status);
			  	}
			});
			
		}
		var homeView;
		var infowindow
		
		//when successfully getting the current position
		function success(position) {
		
			homeView = new google.maps.LatLng(position.coords.latitude,
                                             position.coords.longitude);
        	addMarker(homeView, "You are here!");
        	map.setCenter(homeView);
            infowindow = new google.maps.InfoWindow({
              map: map,
              position: homeView,
              content: 'You are here!',
              size: new google.maps.Size(10,10),
          });
          
          marker = new google.maps.Marker({
		    	position: homeView,
		    	map: map
			});
          map.setCenter(homeView);
		}
		
		function error(msg) {
		  alert('error: ' + msg);
		}
		
		function addMarker(location) {
			marker = new google.maps.Marker({
		    	position: location,
		    	map: map
			});
		  
			map.panTo(location);
			return marker;
		  
		}
	google.maps.event.addDomListener(window, 'load', initialize);
    </script>

  <div id="map_wrapper">
   		<div id="map_canvas"></div>
  </div>
<!--Event Form!-->
<div id="eventForm">
	<div id="form_wrapper">
	<? echo $this->element('event_form'); ?>
  
	</div>
	
</div>
<form id="search_bar">
	<input id="address" type="search" value="UCLA">
	<input type="submit" value="Search" onclick="codeAddress()">
	<a href="#" id="currentLocation"></a>
</form>
