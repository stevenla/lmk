<?php $this->set('tab_bar', '<span id="navright">' . $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Event.id')), null, __('Are you sure you want to delete "%s"?', $this->Form->value('Event.name'))) . '</span>'); ?>
<ul class="list">
	<lh>Edit Event</lh>
	<li>
		<? echo $this->element('event_form'); ?>
	</li>
</ul>
