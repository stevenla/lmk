<?php $this->set('tab_bar', '<span id="navright"><a href="#">Subscribe</a></span>'); ?>

    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1MJxRNYKyb_-ONBOJt1wUHe03niXTxPw&sensor=true"> 
    </script>

    

    <script type="text/javascript">
		createSub = false;
		alerted = false;
		catSelected = -1;
		circle = null;

		$(document).ready(function() {
			$('#navright a').bind('click', function () {
				$('#navright').addClass('pressed');
				createSub = true;
				if (!alerted) {
					alert ("Tap a location on the map to subscribe to an area around it");
					alerted = true;
				}
			});

			$('#cat li a').bind('click', function () {
				$('#cat li').removeClass('active');
				$(this).parent().addClass('active');
			});

		    $('#cancel').click(function(){
				$("#eventForm").animate({"top": "-400px"}, "slow");
		    	cleanForm();
		    	return false;
		    });

			$('#search_bar').live('submit', function(e){
				codeAddress();
				$('body').scrollTop(1);
				$('#address').blur();
				e.preventDefault();
			});
			
			$('#currentLocation').click(function(){
				 map.panTo(homeView);
			});

			$('#geosearch').click(function(){
	
				codeAddress();
			});

			$('#SubscriptionExploreForm').submit(function(){ 
				var formData = $(this).serialize(); 
				var formUrl = "/users/ajax_subscribe"; 
				$.ajax({ 
					type: 'POST', 
					url: formUrl, 
					data: formData, 
					success: function(data)
					{ 
						data = JSON.parse(data);
						if(data.MSG == "OK"){
							cleanForm();
							$("#eventForm").animate({"top": "-400px"}, "slow");
						}
						else
							alert("Can't create event. Try again!");
						
					}
				});
				return false;
			});

			$('#cat li').click(function(){
				catSelected = $(this).attr('id');
				var sendData = {"cur_long":homeView.lng(),
								"cur_lat" :homeView.lat(),
								"filter_cat_id" : this.id};
				var formUrl = "/events/ajax_around_me"; 
				//alert(".");
				$.ajax({ 
					type: 'POST', 
					url: formUrl, 
					data: sendData, 
					success: function(data)
					{ 
						hideMarkers();
						data = JSON.parse(data);
						//alert(data[0]['Event']['id']);
						for (d in data)
						{
							i=d;
							d=data[d]['Event'];
							marker = new google.maps.Marker({
						        position: new google.maps.LatLng(d['latitude'], d['longitude']),
						        map: map
						      });
						    markersArray.push(marker);
						    markerInfo.push('<a href="/event/' + d['id'] + '" target="_blank"><b>' + d['name'] + '</b></a><br>' + d['start_time'] + '<br>'+d['description']);
						     google.maps.event.addListener(marker, 'click', (function(marker, i) {
						        return function() {
						          eventWindow.setContent(markerInfo[i]);
						          eventWindow.open(map, marker);
						        }
						      })(marker, i));
						}
					}
				});
				return false;
			})
	    })
		  
		function hideMarkers() {
	        for (var i=0; i<markersArray.length; i++) {
	            markersArray[i].setVisible(false); // call the setVisible method of the marker
	        }
	        markersArray=[];
	        markerInfo=[];
	      }

		
		function blick(){
		
		}
		var i;
		var eventWindow = new google.maps.InfoWindow();
		var map;
		var markersArray = new Array();
		var markerInfo    = new Array();
		var createEvent = true;
		var geocoder;
		var searchMarker;
		

		
		function initialize() {
			//var defaultView = new google.maps.LatLng(37.7699298, -122.4469157);
		  	geocoder = new google.maps.Geocoder();
			var mapOptions = {
		    	zoom: 14,
		    	//center: defaultView,
		    	mapTypeControl: false,
		    	streetViewControl: false,
				scaleControl: false,
				zoomControl: false,
				panControl: false,
		    	mapTypeId: google.maps.MapTypeId.ROADMAP
		  	};
		  	map =  new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(success, error);
			} else {
				alert('geolocation not supported');
			}
		  	var s;
<?php 
if(isset($subscription)){
	echo "s = true;";

?>
if( s== true)
{
	var s= new google.maps.LatLng(<?php echo $subscription['Subscription']['latitude']?>,
	<?php echo $subscription['Subscription']['longitude']?>);
	//var c;
//	circle.setVisible(true);

	circle = new google.maps.Circle({
		clickable: false,
		editable: false,
		center: s,
		radius: 2*1609,
		fillColor: '#33FF88',
		map: map,
		fillOpacity: .5,
		visibility: true
	});
	rad = 2;
	cleanForm();
	

}

<?php
}?>
		
		  	
		    //listener functions for creating events
			google.maps.event.addListener(map, 'click', function(e) {
				if(createSub == true){
					if (catSelected == -1) 
					{
						alert ("You must first select a category");
						$('#navright').removeClass('pressed');
						createSub = false;
					}
					else
					{
						if (circle != null)
							circle.setVisible(false);
						circle = new google.maps.Circle({
							clickable: false,
							editable: false,
							center: e.latLng,
							radius: 2*1609,
							fillColor: '#33FF88',
							map: map,
							fillOpacity: .5,
							visibility: true
						});
						rad = 2;
						cleanForm();
						$("#eventForm").animate({"top": "0"}, "slow");
						$('#SubscriptionLongitude').val(e.latLng.lng());
						$('#SubscriptionRadius').val(rad);
						$('#SubscriptionLatitude').val(e.latLng.lat());
						$('#SubscriptionCategoryId').val(catSelected);
						$('#navright').removeClass('pressed');
						createSub = false;
					}
			    }
		    
			});
			
			var buttontest = document.getElementById('button2');
			google.maps.event.addDomListener(buttontest, 'click', function(){
		  		createEvent = true;
		  	});
		}
		function cleanForm(){
			$(':input','#SubscriptionCreateForm')
			 .not(':button, :submit, :reset, :hidden, :select')
			 .val('');
		}
		
		//search location
		function codeAddress() {
			 var address = $("#address").val();
	
			geocoder.geocode( { 'address': address}, function(results, status) {
				
				if (status == google.maps.GeocoderStatus.OK) {

					//map.setCenter(results[0].geometry.location);
					if(!searchMarker){
						searchMarker=addMarker(results[0].geometry.location)
					}else
					{
						
						searchMarker.setPosition(results[0].geometry.location);
					}
					/*var marker = new google.maps.Marker({
			    		map: map, 
			    		position: results[0].geometry.location
			    	});*/
			  	} else {
			    	window.alert("Geocode was not successful for the following reason: " + status);
			  	}
			});
			
		}
		
		var homeView;
		//when successfully getting the current position
		function success(position) {
		
		  homeView = new google.maps.LatLng(position.coords.latitude,
                                             position.coords.longitude);
          var infowindow = new google.maps.InfoWindow({
              map: map,
              position: homeView,
              content: 'You are here!',
              size: new google.maps.Size(10,10),
          });
          
          $('#listView').attr("href", "/explore_list/"+homeView.lat()+"/"+homeView.lng());
          
          marker = new google.maps.Marker({
		    	position: homeView,
		    	map: map
			});
          map.setCenter(homeView);
		}
		
		function error(msg) {
		  alert('error: ' + msg);
		}
		
		function addMarker(location) {
			marker = new google.maps.Marker({
		    	position: location,
		    	map: map
			});
		  
			map.panTo(location);
			return marker;
		  
		}

	google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  <div id="map_wrapper">
   		<div id="map_canvas"></div>

  </div>
  <form id="search_bar">
	<input id="address" type="search" value="UCLA">
	<input id="geosearch" type="submit" value="Search">
 	<a href="#" id="currentLocation"></a>
 	<a href="#" id="listView"></a>
</form>
<style>
	
</style>

<div id="eventForm">
	<div id="form_wrapper">
	<? echo $this->element('subscribe_form'); ?>
  
	</div>
	
</div>
<div id="cat">
	<ul>
	<?php foreach($category_list as $category): ?>
		<li id="<?=$category['Categories']['id']?>" style="background-image:url(/img/<?=$category['Categories']['short_name']?>)"><a href="#"><?=htmlspecialchars($category['Categories']['name'])?></a></li>
	<?php endforeach; ?>
	</ul>
</div>
</div>


