

<style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0;}
      #map_wrapper{height: 400px; width:100%; z-index:1;position: absolute;}
      #map_canvas { height: 100%; width: 100%;}

    </style>
 
    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC1MJxRNYKyb_-ONBOJt1wUHe03niXTxPw&sensor=true"> 
    </script>

    

    <script type="text/javascript">
 

		$(document).ready(function() {
			$('#search_bar').live('submit', function(e){
				codeAddress();
				$('body').scrollTop(1);
				$('#address').blur();
				e.preventDefault();
			});

			$('#currentLocation').click(function(){
				 map.panTo(homeView);
			});

			$('#cat li a').bind('click', function () {
				$('#cat li').removeClass('active');
				$(this).parent().addClass('active');
			});
	    })
		  


		
		function blick(){
		
		}
		
		var map;
		var markersArray = [];
		var createEvent = true;
		var geocoder;

		
		function initialize() {
			//var defaultView = new google.maps.LatLng(37.7699298, -122.4469157);
		  	geocoder = new google.maps.Geocoder();
			var mapOptions = {
		    	zoom: 14,
		    	//center: defaultView,
		    	mapTypeId: google.maps.MapTypeId.ROADMAP
		  	};
		  	map =  new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(success, error);
			} else {
				alert('geolocation not supported');
			}
		  
		    //listener functions for creating events
			google.maps.event.addListener(map, 'click', function(e) {
				if(createEvent == true){
			    	
			    	addMarker(e.latLng);
			    	//createEvent = false;
			    }
		    
			});
			
			var buttontest = document.getElementById('button2');
			google.maps.event.addDomListener(buttontest, 'click', function(){
		  		createEvent = true;
		  	});
		}
		
		//search location
		function codeAddress() {
			 var address = $("#address").val();
			 //window.alert(address);
			geocoder.geocode( { 'address': address}, function(results, status) {
				
				if (status == google.maps.GeocoderStatus.OK) {

					//map.setCenter(results[0].geometry.location);
					addMarker(results[0].geometry.location, "You are here")
					/*var marker = new google.maps.Marker({
			    		map: map, 
			    		position: results[0].geometry.location
			    	});*/
			  	} else {
			    	window.alert("Geocode was not successful for the following reason: " + status);
			  	}
			});
			
		}
		
		var homeView;
		//when successfully getting the current position
		function success(position) {
		
		  homeView = new google.maps.LatLng(position.coords.latitude,
                                             position.coords.longitude);
          var infowindow = new google.maps.InfoWindow({
              map: map,
              position: homeView,
              content: 'You are here!',
              size: new google.maps.Size(10,10),
          });
          
          marker = new google.maps.Marker({
		    	position: homeView,
		    	map: map
			});
          map.setCenter(homeView);
		}
		
		function error(msg) {
		  alert('error: ' + msg);
		}
		
		function addMarker(location) {
			marker = new google.maps.Marker({
		    	position: location,
		    	map: map
			});
		  
			map.panTo(location);
		  
		}

	google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  <div id="map_wrapper">
   		<div id="map_canvas"></div>

  </div>
  <form id="search_bar">
	<input id="address" type="search" value="UCLA">
	<input type="submit" value="Search" onclick="codeAddress()">
	<a href="#" id="currentLocation"></a>
</form>
<script type="text/javascript">
</script>
<div id="cat">
	<ul>
	<?php foreach($category_list as $category): ?>
		<li id="<?=$category['Categories']['id']?>" style="background-image:url(/img/<?=$category['Categories']['short_name']?>)"><a href="#"><?=htmlspecialchars($category['Categories']['name'])?></a></li>
	<?php endforeach; ?>
	</ul>
</div>
</div>
