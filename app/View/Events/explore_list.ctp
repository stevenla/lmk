<ul class="list">
	<lh>Events Around Me:</lh>
	<?php foreach($event_list as $event): ?>
		<li>
			<a href="<?php echo $this->Html->url('/event/'.$event['Event']['id']);?>">
				<strong><?php echo $event['Event']['name']; ?></strong>
				<b>Miles from me:</b> <?php echo $event['Event']['miles_from_me']; ?><br />
				<b>Meeting time:</b> <?php echo $event['Event']['start_time']; ?><br />
				<em><?php echo $event['Event']['description']; ?></em>
			</a>
		</li>
<?php endforeach; ?>
</ul>
<div id="cat">
	<ul>
	<?php foreach($category_list as $category): ?>
		<li id="<?=$category['Categories']['id']?>" style="background-image:url(/img/<?=$category['Categories']['short_name']?>)"><a href="#"><?=htmlspecialchars($category['Categories']['name'])?></a></li>
	<?php endforeach; ?>
	</ul>
</div>
</div>


