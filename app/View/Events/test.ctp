<?php echo $this->Form->create('Event', array('class' => 'eventForm'));?>
<fieldset>
<?php
	echo $this->Form->input('name', array('label' => 'Event Name'));
	echo $this->Form->input('longitude', array('type' => 'hidden'));
	echo $this->Form->input('latitude', array('type' => 'hidden'));
	echo $this->Form->input('category_id');
	echo '<label for="EventStartTime">Date & Time</label><input type="datetime" name="data[Event][start_time_a]" id="EventStartTime"/>';
	echo $this->Form->input('description');
?>
</fieldset>
<input type="button" value="Cancel" class="left" id="cancel"/>
<?php echo $this->Form->submit('Submit'); ?>
<div class="clear"></div>
<?php echo $this->Form->end();?>
