<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1" />
	<title> <?php echo $title_for_layout; ?> - lmk</title>
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="apple-touch-icon-precomposed" href="/img/icon114.png" />
<?php echo $this->Html->css('default'); ?>
<?php echo $this->Html->script('jquery-1.7.1.min'); ?>
<?php echo $this->Html->script('default'); ?>
<?php echo $scripts_for_layout; ?>
</head>
<body>
	<div id="nav">
		<h1><a href="/"><span>lmk</span></a></h1>
		<?php if (!$logged_in): ?>
			<span id="navleft"><a href="<?php echo $this->Html->url('/login');?>">Log In</a></span>
		<?php else: ?>
			<span id="navleft"><a href="<?php echo $this->Html->url('/logout');?>">Log Out</a></span>
		<?php endif; ?>
		<?php if (isset($tab_bar)) echo $tab_bar; ?>
		
	</div>
	<div id="body">
<?php echo $this->Session->flash();?>
<?php echo $this->Session->flash('auth');?>
<?php echo $content_for_layout; ?>
	</div>
	
	<div id="sql_dump">
	<?php /*echo $this->element('sql_dump'); */?>
	</div>
</body>
</html>
