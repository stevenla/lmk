<?php if($logged_in): ?>
	<a href="<?php echo $this->Html->url('/explore');?>" class="button blue">Explore Around Me</a>
	<a href="<?php echo $this->Html->url(array('controller'=>'events', 'action'=>'create'));?>" class="button blue">Post an Event</a>
	<a href="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'my_events'));?>" class="button blue">My Events</a>
	<ul class="list">
		<lh>My Subscriptions:</lh>
		
<?php foreach($subscription_arr as $s): ?>
		<li>
			<a href="/explore/<? echo $s['Subscription']['id']?>">
			
				<strong> <?echo $s['Subscription']['name']; ?> - (<? echo $s['Category']['name']; ?>)</strong>
				<em>radius: <? echo $s['Subscription']['radius']; ?> miles</em>
			</a>
		</li>
<?php endforeach; ?>
	</ul>

<?php else: ?>
<?php $this->set('tab_bar', '<span id="navright"><a href="/users/add">Register</a></span>'); ?>
<div id="splash">

	<div id="within">

		<h1>Welcome to LMK.</h1>
		<p>Never miss a beat again.</p>
		<p>People around you are useful, just you don't know. Whether you're looking for something from someone or with someone, LMK will help make these connections.</p>
		<p>The possibilities could be finding casual sporting matches, scoring someone's spare tickets to a concert, finding someone willing to swipe your dorm food....</p>

		<p></p>

	</div>

</div>

<?php endif; ?>
