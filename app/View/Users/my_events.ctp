<ul class="list">
	<lh>Events I've Created:</lh>
	<?php foreach($event_arr as $event): ?>
		<li>
			<a href="<?php echo $this->Html->url('/event/'.$event['Event']['id']);?>">
				<strong><?php echo $event['Event']['name']; ?></strong>
				<em><?php echo $event['Event']['start_time']; ?></em>
				<em><?php echo $event['Event']['description']; ?></em>
			</a>
		</li>
<?php endforeach; ?>
</ul>